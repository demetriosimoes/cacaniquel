package com.company;

import java.util.ArrayList;

public class Calculo {

    private ArrayList<Integer> listaPontos = new ArrayList<Integer>();

    public void setListaPontos(ArrayList<Integer> listaPontos) {
        this.listaPontos = listaPontos;
    }

    private boolean tresValoresIguais(int slots){
        int num1,num2;
        boolean bret=false;
        for(int i=0;i<slots;i++){
            num1=this.listaPontos.get(i);
            for(int j=0;j<slots;j++){
                num2=listaPontos.get(j);
                if(num1!=num2){
                    return false;
                }
            }
        }
        return true;
    }
    public Integer getpontos(int slots){
        int soma=0;
        for(int i=0;i<slots;i++){
            soma= soma + this.listaPontos.get(i);
        }
        //System.out.println("soma:" +soma);
        //System.out.println(this.tresValoresIguais(slots));
        if(this.tresValoresIguais(slots)){
            soma=soma*100;
        }
        return soma;
    }
}
