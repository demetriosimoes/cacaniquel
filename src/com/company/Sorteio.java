package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.TreeMap;

public class Sorteio {
    //aqui para transferir de TreeMap para ArrayList
    private TreeMap<String,Integer> simbolos = new TreeMap<String,Integer>();
    private ArrayList<String> listaSorteioOpcoesNome = new ArrayList<String>();
    private ArrayList<Integer> listaSorteioValor = new ArrayList<Integer>();
    //Aqui para armazenar a opçao e o valor sorteado
    private ArrayList<String> OpcoesSorteadasNome = new ArrayList<String>();
    private ArrayList<Integer> OpcoesSorteadasValor = new ArrayList<Integer>();

    public ArrayList<String> getOpcoesSorteadasNome() {
        return OpcoesSorteadasNome;
    }

    public ArrayList<Integer> getOpcoesSorteadasValor() {
        return OpcoesSorteadasValor;
    }

    public Sorteio(TreeMap<String, Integer> simbolos) {
        this.simbolos = simbolos;
    }

    public void realizaSorteio(int slots){
        int i=0;
        //TrueMap para Array
        for(String opcoesNome:this.simbolos.keySet())
        {
            this.listaSorteioOpcoesNome.add(opcoesNome);
            this.listaSorteioValor.add(simbolos.get(opcoesNome));
            //System.out.println("a:"+opcoesNome);
            //System.out.println(simbolos.get(opcoesNome));
            i++;
        }
        //sorteio
        Random gerador = new Random();
        int j=0;
        for(int k=0;k<slots;k++){
            j =gerador.nextInt(i);
            this.OpcoesSorteadasNome.add(this.listaSorteioOpcoesNome.get(j));
            this.OpcoesSorteadasValor.add(this.listaSorteioValor.get(j));
        }
        //System.out.println(this.listaSorteioOpcoesNome);
        //System.out.println(this.simbolos);
    }
}
