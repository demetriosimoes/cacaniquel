package com.company;

import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
	    // exibe os simbolos

        Integer slots = 3;
        Impressora imprime = new Impressora();
        Constantes constantes = new Constantes();

        TreeMap<String,Integer> simbolos = new TreeMap<String,Integer>();
        Simbolos sb = new Simbolos(simbolos);
        //System.out.println(sb.exibeimbolos);

        //sorteio
        Sorteio sorteio = new Sorteio(sb.exibeimbolos);
        sorteio.realizaSorteio(slots);
        imprime.imprimir(constantes.OPCOES_SORTEADAS + sorteio.getOpcoesSorteadasNome());
        imprime.imprimir(constantes.PONTOS_SORTEADAS + sorteio.getOpcoesSorteadasValor());

        //calculopontos
        Calculo pontos = new Calculo();
        pontos.setListaPontos(sorteio.getOpcoesSorteadasValor());
        Integer qtdePontos = pontos.getpontos(slots);
        imprime.imprimir(constantes.PONTOS+qtdePontos);
    }



}
